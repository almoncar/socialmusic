package es.babel.ema.presentation.ui.splash

import com.carmabs.ema.core.state.EmaBaseState
import es.babel.domain.STRING_EMPTY

/**
 * Splash view state
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

data class SplashState(val version:String = STRING_EMPTY) : EmaBaseState {
}
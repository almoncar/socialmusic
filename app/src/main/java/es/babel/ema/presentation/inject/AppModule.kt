package es.babel.ema.presentation.inject

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.provider.UserDictionary.Words.APP_ID
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton


/**
 * Injection for app
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */


fun generateAppModule(app: Application) = Kodein.Module(name = "AppModule") {

    bind<Application>() with singleton { app }

    bind<Resources>() with singleton { app.resources }


    bind<SharedPreferences>() with singleton { app.getSharedPreferences(APP_ID, Context.MODE_PRIVATE) }


}

package es.babel.ema.presentation.base

import com.carmabs.ema.core.navigator.EmaNavigationState
import com.carmabs.ema.core.state.EmaState
import es.babel.ema.presentation.ui.MainToolbarsViewModel

/**
 * Base ViewModel for LDA application with toolbars access management
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

abstract class BaseToolbarsViewModel<T, NS : EmaNavigationState> : BaseViewModel<T, NS>() {

    lateinit var mainToolbarsVm: MainToolbarsViewModel

    override fun onStart(inputState: EmaState<T>?): Boolean {
        val firstTime = super.onStart(inputState)
        onConfigureToolbars(mainToolbarsVm)
        return firstTime
    }

    abstract fun onConfigureToolbars(mainToolbarsVm: MainToolbarsViewModel)
}
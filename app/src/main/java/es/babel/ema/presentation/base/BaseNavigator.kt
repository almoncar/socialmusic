package es.babel.ema.presentation.base

import com.carmabs.ema.android.navigation.EmaNavigator
import com.carmabs.ema.core.navigator.EmaNavigationState


/**
 * Base navigator for app
 *
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

abstract class BaseNavigator<NS : EmaNavigationState> : EmaNavigator<NS> {

    sealed class Navigation : EmaNavigationState

}
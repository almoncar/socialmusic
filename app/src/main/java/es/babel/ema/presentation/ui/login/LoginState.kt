package es.babel.ema.presentation.ui.login

import com.carmabs.ema.core.state.EmaBaseState
import es.babel.domain.STRING_EMPTY

/**
 * Home view state
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

data class LoginState(
        val buttonLoginVisibility: Boolean = true,
        val buttonLoginEnable: Boolean = true) : EmaBaseState
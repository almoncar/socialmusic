package es.babel.ema.presentation.base

import com.carmabs.ema.android.base.EmaApplication
import es.babel.ema.presentation.inject.generateAppModule
import org.kodein.di.Kodein


/**
 * App instance for the application. The module for dependency injection are set here
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

class MakeMoney :  EmaApplication() {

    override fun injectAppModule(kodein: Kodein.MainBuilder): Kodein.Module = setupAppModules(kodein)

    override val kodein: Kodein
        get() = super.kodein.apply { Kodein }

    private fun setupAppModules(kodein: Kodein.MainBuilder): Kodein.Module {
        return generateAppModule(this)
    }

    override fun onCreate() {
        super.onCreate()
        //Stetho.initializeWithDefaults(this)
    }
}
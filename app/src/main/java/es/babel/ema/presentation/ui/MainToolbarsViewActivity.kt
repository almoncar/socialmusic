package es.babel.ema.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.carmabs.ema.R
import com.carmabs.ema.android.ui.EmaView
import com.carmabs.ema.core.state.EmaExtraData
import es.babel.domain.STRING_EMPTY
import es.babel.ema.presentation.base.BaseActivity
import es.babel.ema.presentation.model.BackModel
import es.babel.ema.presentation.model.TabbarModel
import es.babel.ema.presentation.model.ToolbarModel
import es.babel.ema.presentation.navigation.MainNavigator
import kotlinx.android.synthetic.main.activity_base.emaAppBarLayout
import kotlinx.android.synthetic.main.activity_base.navHostFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.generic.instance


class MainToolbarsViewActivity : BaseActivity(), EmaView<HomeToolbarsState, MainToolbarsViewModel, MainNavigator.Navigation> {
    override fun onSingleEvent(data: EmaExtraData) {
    }

    override val viewModelSeed: MainToolbarsViewModel by instance()

    override val navigator: MainNavigator by instance()

    override val inputState: HomeToolbarsState? = null


    private var bottomViewMargin: Int = 0

    private var backModel: BackModel? = null

    private var defaultElevation = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeViewModel(this)
        emaAppBarLayout.elevation = 0f
        bottomViewMargin = (navHostFragment.view?.layoutParams as? ConstraintLayout.LayoutParams)?.bottomMargin
                ?: 0
    }

    private var vm: MainToolbarsViewModel? = null

    override fun onViewModelInitalized(viewModel: MainToolbarsViewModel) {
        vm = viewModel
        defaultElevation = resources.getDimension(R.dimen.elevation_top)
        setupBottomSheet(viewModel)
        setupToolbar(viewModel)
    }

    private fun setupToolbar(viewModel: MainToolbarsViewModel) {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            val backVisibility = destination.id != R.id.LoginViewFragment
            //False to avoid screen update and change title effect flash
            viewModel.onActionUpdateToolbar(false) {
                it.copy(
                        backVisibility = backVisibility,
                        title = destination.label?.toString() ?: STRING_EMPTY
                )
            }
        }
        clToolbarBack.setOnClickListener { viewModel.onActionBackClicked() }
        ivToolbarCloseSession.setOnClickListener { viewModel.onActionCloseSessionClicked() }
    }

    private fun setupBottomSheet(viewModel: MainToolbarsViewModel) {

    }

    override fun onBackPressed() {
        backModel?.let {
            if (!it.disabled) {
                checkBackImplementation()
            }
        } ?: onBackSystemPressed()

    }

    private fun checkBackImplementation() {

        backModel?.implementation?.invoke() ?: onBackSystemPressed()
    }

    override fun onStateNormal(data: HomeToolbarsState) {
        checkTabbarState(data.tabbarModel.expanded)
        if (checkToolbarVisibility(data)) {
            updateToolbar(data.toolbarModel)
        }
        checkTabbarVisibility(data)
        updateTabbar(data.tabbarModel)
        backModel = data.backModel
    }

    private fun updateTabbar(tabbarModel: TabbarModel) {

    }

    private fun checkTabbarState(tabbarExpanded: Boolean) {
    }

    private fun checkTabbarVisibility(data: HomeToolbarsState) {
        if (data.tabbarModel.visibility)
            showTabbar()
        else
            hideTabbar()
    }

    private fun checkToolbarVisibility(data: HomeToolbarsState): Boolean {
        val visibility = data.toolbarModel.visibility
        val gone = data.toolbarModel.gone

        if (visibility)
            showToolbar()
        else
            hideToolbar(gone)

        return visibility
    }

    private fun updateToolbar(data: ToolbarModel) {
        val title = data.title
        val backVisibility = if (data.backVisibility) View.VISIBLE else View.INVISIBLE
        val closeSessionVisibility = if (data.closeSessionVisibility) View.VISIBLE else View.INVISIBLE
        if (title.isEmpty()) {
            tvToolbarTitle.visibility = View.GONE
            ivToolbarLogo.visibility = View.VISIBLE

        } else {
            ivToolbarLogo.visibility = View.GONE
            tvToolbarTitle.visibility = View.VISIBLE
        }

        if (data.elevation) {
            toolbarLayout.elevation = defaultElevation
        } else
            toolbarLayout.elevation = 0f

        tvToolbarTitle.text = title
        clToolbarBack.visibility = backVisibility
        ivToolbarCloseSession.visibility = closeSessionVisibility

        data.exitButton?.let { exitButton ->
            tvToolbarExit.visibility = View.VISIBLE
            tvToolbarExit.text = exitButton.text
            tvToolbarExit.setOnClickListener {
                exitButton.onClickListener.invoke()
            }
        }

        data.backClickListener?.let { listener ->
            clToolbarBack.setOnClickListener { listener.invoke() }
        }

        if (data.backDrawableCross) {
            ivToolbarBack.setImageResource(R.drawable.ic_cross)
        } else {

        }
    }


    override fun onStateLoading(data: EmaExtraData) {

    }

    override fun onStateError(error: Throwable) {

    }

    private fun onBackSystemPressed() {
        vm?.onActionBackClicked()
    }

    override fun getToolbarTitle(): String? = STRING_EMPTY

    override fun getNavGraph(): Int = R.navigation.navigation_main

    override fun getLayout(): Int = R.layout.activity_main

    private fun showTabbar() {
        (navHostFragment.view?.layoutParams as? ConstraintLayout.LayoutParams)?.bottomMargin = bottomViewMargin
        clTabbar.visibility = View.VISIBLE
    }

    private fun hideTabbar() {
        (navHostFragment.view?.layoutParams as? ConstraintLayout.LayoutParams)?.bottomMargin = 0
        clTabbar.visibility = View.GONE
    }
}
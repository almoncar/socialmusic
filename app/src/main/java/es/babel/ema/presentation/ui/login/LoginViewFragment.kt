package es.babel.ema.presentation.ui.login

import android.view.View
import com.carmabs.ema.R
import com.carmabs.ema.core.state.EmaExtraData
import es.babel.ema.presentation.base.BaseToolbarsFragment
import es.babel.ema.presentation.ui.MainToolbarsViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import org.kodein.di.generic.instance

class LoginViewFragment : BaseToolbarsFragment<LoginState, LoginViewModel, LoginNavigator.Navigation>() {

    private lateinit var viewModel: LoginViewModel

    override fun onInitializedWithToolbarsManagement(viewModel: LoginViewModel, mainToolbarViewModel: MainToolbarsViewModel) {
        this.viewModel = viewModel
        button.setOnClickListener {
            viewModel.cambiarBoton()
        }
    }

    override fun onLoading(data: EmaExtraData) {
    }

    override fun onError(error: Throwable): Boolean {
        return false
    }

    override val inputStateKey: String? = null

    override fun getFragmentLayout(): Int = R.layout.fragment_login

    override val viewModelSeed: LoginViewModel by instance()

    override val navigator: LoginNavigator by instance()

    override fun onSingleEvent(data: EmaExtraData) {
    }

    override fun onNormal(data: LoginState) {
        button.visibility = if (data.buttonLoginVisibility) View.VISIBLE else View.GONE
        button.isEnabled = data.buttonLoginEnable
    }


}
package es.babel.ema.presentation.ui

import com.carmabs.ema.core.state.EmaBaseState
import es.babel.ema.presentation.model.BackModel
import es.babel.ema.presentation.model.TabbarModel
import es.babel.ema.presentation.model.ToolbarModel

/**
 * Home view state
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

data class HomeToolbarsState(
        val tabbarModel: TabbarModel = TabbarModel(),
        val toolbarModel: ToolbarModel = ToolbarModel(),
        val backModel: BackModel = BackModel()
) : EmaBaseState
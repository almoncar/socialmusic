package es.babel.ema.presentation.base

import com.carmabs.ema.android.viewmodel.EmaViewModel
import com.carmabs.ema.core.navigator.EmaNavigationState
import com.carmabs.ema.core.state.EmaState


/**
 * Base ViewModel for LDA application
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

abstract class BaseViewModel<T, NS : EmaNavigationState> : EmaViewModel<T, NS>() {


    override fun onStart(inputState: EmaState<T>?): Boolean {
        val firsTime = super.onStart(inputState)
        onStartAnalytic()
        return firsTime
    }

    override fun onStartFirstTime(statePreloaded: Boolean) {
        // Override when is needed to launch first time options
    }

    abstract fun onStartAnalytic()
}
package es.babel.ema.presentation.ui.login

import androidx.navigation.NavController
import com.carmabs.ema.R
import com.carmabs.ema.core.navigator.EmaBaseNavigator
import com.carmabs.ema.core.navigator.EmaNavigationState
import es.babel.ema.presentation.base.BaseNavigator

/**
 * Navigation on Home
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

class LoginNavigator(override val navController: NavController) : BaseNavigator<LoginNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState {

        object ToNext : LoginNavigator.Navigation() {
            override fun navigateWith(navigator: EmaBaseNavigator<out EmaNavigationState>) {
                val nav = navigator as LoginNavigator
                nav.toNext()
            }
        }
    }

    private fun toNext() {
        navigateWithAction(R.id.action_LoginViewFragment_to_mainScreenViewFragment)
    }


}
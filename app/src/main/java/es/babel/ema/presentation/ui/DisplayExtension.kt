package com.babel.lineadirecta.ui

import android.content.Context
import android.util.TypedValue

/**
 * Utils for display stuff
 *
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

fun Int.toDp(context: Context):Int {
    return Math.round(TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,toFloat(),context.resources.displayMetrics))
}

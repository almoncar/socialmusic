package es.babel.ema.presentation.base

import android.content.Context
import android.os.SystemClock
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.babel.lineadirecta.DEFAULT_CLICK_LISTENER_THRESHOLD
import com.carmabs.ema.android.ui.EmaFragment
import com.carmabs.ema.core.navigator.EmaNavigationState
import com.carmabs.ema.core.state.EmaBaseState
import com.carmabs.ema.core.state.EmaExtraData
import es.babel.ema.presentation.generateFragmentModule
import org.kodein.di.Kodein

/**
 * Base fragment for LDA application
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

abstract class BaseFragment<S : EmaBaseState, VM : BaseViewModel<S, NS>, NS : EmaNavigationState> : EmaFragment<S, VM, NS>() {

    override fun injectFragmentModule(kodein: Kodein.MainBuilder): Kodein.Module = generateFragmentModule(this)

    override val fragmentViewModelScope: Boolean
        get() = true

    private var lastTimeClicked: Long = 0

    override fun onStateError(error: Throwable) {
    }

    override fun onStateLoading(data: EmaExtraData) {
        onLoading(data)
    }

    override fun onStateNormal(data: S) {
        onNormal(data)
    }

    /**
     * We create this abstract function if we want to appy default behaviours in [onStateNormal],[onStateError],[onStateLoading]
     * @param data
     */
    abstract fun onLoading(data: EmaExtraData)

    abstract fun onNormal(data: S)

    abstract fun onError(error: Throwable): Boolean

    override fun onNavigation(navigation: EmaNavigationState) {
        super.onNavigation(navigation)
        context?.let {
            val hideKeyboard: InputMethodManager = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            view?.let { view ->
                hideKeyboard.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }

    protected fun View.setOnClickListenerForNavigation(defaultClickIntervalMillis: Long = DEFAULT_CLICK_LISTENER_THRESHOLD, function: (View) -> Unit) {
        setOnClickListener {
            if (SystemClock.elapsedRealtime() - lastTimeClicked >= defaultClickIntervalMillis) {
                lastTimeClicked = SystemClock.elapsedRealtime()
                function.invoke(this)
            }
        }

    }
}


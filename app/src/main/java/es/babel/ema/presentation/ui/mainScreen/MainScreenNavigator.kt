package es.babel.ema.presentation.ui.mainScreen

import android.app.Activity
import androidx.navigation.NavController
import com.carmabs.ema.core.navigator.EmaNavigationState
import es.babel.ema.presentation.base.BaseNavigator

/**
 * Navigation on Home
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

class MainScreenNavigator(override val navController: NavController, private val activity: Activity) : BaseNavigator<MainScreenNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState {

    }
}
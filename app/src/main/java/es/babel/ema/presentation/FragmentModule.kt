package es.babel.ema.presentation

import androidx.fragment.app.Fragment
import es.babel.ema.presentation.ui.login.LoginViewModel
import es.babel.ema.presentation.ui.mainScreen.MainScreenViewModel
import es.babel.ema.presentation.ui.splash.SplashViewModel
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


/**
 * Injection for fragments
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */


fun generateFragmentModule(fragment: Fragment) = Kodein.Module(name = "FragmentModule") {

    //FRAGMENT//

    bind<Fragment>() with provider { fragment }

    //FRAGMENT MANAGER//


    //VIEW MODEL//

    bind<SplashViewModel>() with provider { SplashViewModel() }

    bind<LoginViewModel>() with singleton { LoginViewModel() }

    bind<MainScreenViewModel>() with singleton { MainScreenViewModel() }
}
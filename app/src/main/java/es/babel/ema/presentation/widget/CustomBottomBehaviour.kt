package es.babel.ema.presentation.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior


/**
 * Class to handle touch behaviour events
 *
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */
//
//class CustomBottomBehaviour<V : View> : BottomSheetBehavior<V> {
//
//    constructor() : super()
//    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
//
//    var onNestedClickEvent : (()->Unit)?=null
//}
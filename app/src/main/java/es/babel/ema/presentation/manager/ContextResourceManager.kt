package es.babel.ema.presentation.manager

import android.content.Context
import es.babel.domain.manager.ResourceManager

/**
 * Context implementation for resource manager
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

class ContextResourceManager(private val context: Context) : ResourceManager {

}
package es.babel.ema.presentation.ui

import com.carmabs.ema.core.state.EmaExtraData
import es.babel.domain.FLOAT_ZERO
import es.babel.ema.presentation.base.BaseViewModel
import es.babel.ema.presentation.model.ActivityResultHandlerModel
import es.babel.ema.presentation.model.BackModel
import es.babel.ema.presentation.model.TabbarModel
import es.babel.ema.presentation.model.ToolbarModel
import es.babel.ema.presentation.navigation.MainNavigator


class MainToolbarsViewModel(
) : BaseViewModel<HomeToolbarsState, MainNavigator.Navigation>() {

    companion object {
        const val SINGLE_ACTIVITY_RESULT_HANDLER_ADD = 1
        const val SINGLE_ACTIVITY_RESULT_HANDLER_REMOVE = 2
    }

    override fun onStartAnalytic() {
    }

    override fun createInitialViewState(): HomeToolbarsState {
        return HomeToolbarsState()
    }

    fun onActionBackClicked() {
        navigate(MainNavigator.Navigation.GoBack)
    }

    fun onActionShowTabbar(show: Boolean) {
        onActionUpdateTabbar {
            it.copy(visibility = show)
        }
    }

    fun onActionShowElevation(show: Boolean) {
        updateViewState {
            copy(toolbarModel = toolbarModel.copy(elevation = show))
        }
    }

    fun onActionShowToolbar(show: Boolean, gone: Boolean = true) {
        updateViewState {
            copy(toolbarModel = toolbarModel.copy(visibility = show, gone = gone))
        }
    }

    fun onActionMainTabbarButtonClicked() {
        updateViewState {
            copy(tabbarModel = tabbarModel.copy(expanded = !tabbarModel.expanded))
        }
    }

    fun onActionUpdateToolbar(update: Boolean = true, updateToolbar: (ToolbarModel) -> ToolbarModel) {
        checkViewState {
            updateViewState(update) {
                copy(toolbarModel = updateToolbar.invoke(it.toolbarModel))
            }
        }
    }

    fun onActionUpdateTabbar(updateTabbar: ((TabbarModel) -> TabbarModel)? = null) {

        updateTabbar?.also {
            updateViewState {
                val tabbarUpdated = it.invoke(tabbarModel)
                copy(
                        tabbarModel = tabbarUpdated,
                        toolbarModel = toolbarModel.copy(closeSessionVisibility = tabbarUpdated.visibility)
                )
            }
        } ?: updateViewState()
    }

    fun onActionHandleBack(update: (currentBackModel: BackModel) -> BackModel) {
        checkViewState {
            updateViewState {
                copy(backModel = update.invoke(backModel))
            }
        }
    }

    fun onTabbarSlideAction(offset: Float) {
        if (offset <= FLOAT_ZERO)
            updateViewState {
                copy(tabbarModel = tabbarModel.copy(expanded = false))
            }
    }

    fun onActionCloseSessionClicked() {
        loading()
    }

    fun onActionDialogCloseSessionAccepted() {
    }

    fun onActionDialogCloseSessionCanceled() {
        updateViewState()
    }

    fun addActivityResultHandler(activityResultHandlerModel: ActivityResultHandlerModel) {
        sendSingleEvent(EmaExtraData(SINGLE_ACTIVITY_RESULT_HANDLER_ADD, activityResultHandlerModel))
    }

    fun removeActivityResultHandler(id: Int) {
        sendSingleEvent(EmaExtraData(SINGLE_ACTIVITY_RESULT_HANDLER_REMOVE, id))
    }
}
package es.babel.ema.presentation.ui.mainScreen

import com.carmabs.ema.core.state.EmaBaseState
import es.babel.domain.STRING_EMPTY

/**
 * Home view state
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

data class MainScreenState(
        val loginTitle: String = STRING_EMPTY,
        val loginAccessText: String = STRING_EMPTY,
        val firsTimeText: String = STRING_EMPTY,
        val showContent: Boolean = false,
        val permissionRequest: Boolean = true) : EmaBaseState
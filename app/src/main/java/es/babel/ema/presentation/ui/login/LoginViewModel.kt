package es.babel.ema.presentation.ui.login


import es.babel.ema.presentation.base.BaseToolbarsViewModel
import es.babel.ema.presentation.ui.MainToolbarsViewModel

class LoginViewModel : BaseToolbarsViewModel<LoginState, LoginNavigator.Navigation>() {

    override fun onStartAnalytic() {
    }

    override fun createInitialViewState(): LoginState = LoginState()

    override fun onConfigureToolbars(mainToolbarsVm: MainToolbarsViewModel) {
        mainToolbarsVm.onActionShowToolbar(show = true, gone = true)
        mainToolbarsVm.onActionUpdateToolbar {
            it.copy(
                    backVisibility = false
            )

        }
        mainToolbarsVm.onActionShowTabbar(true)
    }

    fun onActionNavigateNext() {
        navigate(LoginNavigator.Navigation.ToNext)
    }

    fun cambiarBoton() {
        checkViewState {
            updateViewState {
                it.copy(
                        buttonLoginVisibility = !buttonLoginVisibility
                )
            }
        }
    }

}
package es.babel.ema.presentation.inject

import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import es.babel.ema.presentation.ui.MainToolbarsViewModel
import es.babel.ema.presentation.base.BaseActivity
import es.babel.ema.presentation.navigation.MainNavigator
import es.babel.ema.presentation.navigation.SplashNavigator
import es.babel.ema.presentation.ui.login.LoginNavigator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


/**
 * Injection for activities
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */


fun injectionActivityModule(activity: BaseActivity) = Kodein.Module(name = "ActivityModule") {

    //NAVIGATOR//

    bind<SplashNavigator>() with singleton { SplashNavigator(instance(), instance()) }

    bind<MainNavigator>() with singleton { MainNavigator(instance()) }

    bind<FragmentManager>() with provider { activity.supportFragmentManager }

    bind<MainToolbarsViewModel>() with provider { MainToolbarsViewModel() }

    bind<LoginNavigator>() with singleton { LoginNavigator(instance()) }

    //ACTIVITY//

    bind<BaseActivity>() with singleton { activity }

    //NAV CONTROLLER//

    bind<NavController>() with singleton { activity.navController }

    //HANDLERS


}

package es.babel.ema.presentation.navigation

import android.app.Activity
import androidx.navigation.NavController
import com.carmabs.ema.R
import com.carmabs.ema.core.navigator.EmaBaseNavigator
import com.carmabs.ema.core.navigator.EmaNavigationState
import es.babel.ema.presentation.base.BaseNavigator

/**
 * Navigation on Home
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

class SplashNavigator(override val navController: NavController,private val activity: Activity) : BaseNavigator<SplashNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState {

        object LoginFromSplash : Navigation() {
            override fun navigateWith(navigator: EmaBaseNavigator<out EmaNavigationState>) {
                val nav = navigator as SplashNavigator
                nav.toLoginFromSplash()
            }
        }
    }

    private fun toLoginFromSplash() {
        navigateWithAction(R.id.action_splashViewFragment_to_homeViewActivity)
        activity.finish()
    }

}
package es.babel.ema.presentation.widget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.babel.lineadirecta.FONT_GOTHAM_MEDIUM
import com.carmabs.ema.R
import com.carmabs.ema.android.ui.EmaBaseLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import es.babel.domain.INT_NEGATIVE
import es.babel.domain.INT_ZERO
import es.babel.ema.presentation.model.TabbarModel
import es.babel.ema.presentation.model.TabbarTextExpandedModel
import kotlinx.android.synthetic.main.layout_tabbar.view.*


/**
 * Project: LDA
 * Created by: cmateob on 14/12/18.
 */
class TabbarWidget : EmaBaseLayout {

    constructor(context: Context) : super(context)
    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
    constructor(ctx: Context, attrs: AttributeSet, defStyleAttr: Int) : super(ctx, attrs, defStyleAttr)

    enum class TabbarButton {
        LEFT,
        MEDIUM_LEFT,
        MEDIUM,
        MEDIUM_RIGHT,
        RIGHT
    }


    private var heightExpand: Int? = null

    private var expandedOptions = 0

    private val lateralButtons: List<TextViewToogleWidget> by lazy {
        listOf(
                tvTabbarInit,
                tvTabbarMyPolicies,
                tvTabbarReports,
                tvTabbarCrane
        )
    }

    override fun setupAttributes(ta: TypedArray) {

    }

    private fun adjustHeight() {
        if (heightExpand == null) {
            heightExpand = clTabbarOptionsExpanded.height
            layoutParams.height = clTabbarOptionsExpanded.height
            requestLayout()
        }

    }

    override fun getAttributes(): IntArray? = null

    override fun setup(mainLayout: View) {
        lateralButtons.apply {
            forEach {
                val otherViews = lateralButtons.filterNot { view -> view == it }.toTypedArray()
                it.addIncompatibleType(*otherViews)
            }
        }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        adjustHeight()
        return false
    }

    fun attachToBottomSheetBehaviour(behavior: BottomSheetBehavior<*>, slideListener: ((Float) -> Unit)? = null) {
        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(view: View, slideOffset: Float) {
                val clHeight = clTabbarOptions.height
                ivTabbarLda.alpha = 1 - slideOffset
                ivTabbarArrowDown.alpha = slideOffset

                clTabbarOptions.alpha = 1 - slideOffset
                clTabbarOptionsExpanded.translationY = -clHeight * slideOffset

                slideListener?.invoke(slideOffset)
            }

            override fun onStateChanged(p0: View, state: Int) {
            }
        })
    }

    fun configureCollapsedButtons(tabbarModel: TabbarModel) {
        if (!tabbarModel.buttons.isEmpty()) {
            lateralButtons.zip(tabbarModel.buttons).forEach {
                val textView = it.first
                val model = it.second
                textView.text = model.text
                if (model.icon != INT_NEGATIVE)
                    textView.setCompoundDrawablesWithIntrinsicBounds(INT_ZERO, model.icon, INT_ZERO, INT_ZERO)
                it.first.apply {
                    textView.onTextClickListener = model.clickListener
                }
                textView.select(model.selected, false)

            }
        }
    }

    private fun resetHeight() {
        heightExpand = null
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
    }

    fun configureExpandedTexts(tabbarModel: TabbarModel) {

        if (!tabbarModel.expandedTexts.isEmpty()) {

            clTabbarOptionsExpanded.removeAllViews()

            val expandedPairs = tabbarModel.expandedTexts.map {
                Pair(createTextView(), it)
            }

            //RESET HEIGHT FOR NEW CONFIGURATION
            if (expandedOptions != 0 && expandedPairs.size != expandedOptions) {
                resetHeight()
            }

            expandedOptions = expandedPairs.size
            //We set the first time to parent//

            val initial = expandedPairs[INT_ZERO]
            val textView = initial.first
            val model = initial.second
            textView.text = model.text
            textView.setOnClickListener {
                model.clickListener?.invoke()
            }

            val layoutParams = ConstraintLayout.LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            layoutParams.topMargin = resources.getDimension(R.dimen.space_big).toInt()
            layoutParams.startToStart = clTabbarOptionsExpanded.id
            layoutParams.endToEnd = clTabbarOptionsExpanded.id
            layoutParams.topToTop = clTabbarOptionsExpanded.id
            clTabbarOptionsExpanded.addView(textView, layoutParams)

            //We set the constraint for the rest cosidering the item before

            val marginTop = resources.getDimension(R.dimen.space_very_big).toInt()
            expandedPairs.reduce { previous, next ->
                addExpandedText(next, previous, marginTop, if (next == expandedPairs.last()) marginTop else INT_ZERO)
            }
        }
    }

    private fun createTextView(): TextView {
        return TextView(clTabbarOptionsExpanded.context, null).apply {
            id = hashCode()
            val font = Typeface.createFromAsset(context.assets,
                    FONT_GOTHAM_MEDIUM)
            typeface = font
        }
    }

    private fun addExpandedText(current: Pair<TextView, TabbarTextExpandedModel>,
                                previous: Pair<TextView, TabbarTextExpandedModel>,
                                marginTop: Int = INT_ZERO,
                                marginBottom: Int = INT_ZERO): Pair<TextView, TabbarTextExpandedModel> {

        val textView = current.first
        val model = current.second

        textView.text = model.text
        textView.setOnClickListener { model.clickListener?.invoke() }
        val layoutParams = ConstraintLayout.LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.topMargin = marginTop
        textView.setPadding(0, 0, 0, marginBottom)
        layoutParams.startToStart = clTabbarOptionsExpanded.id
        layoutParams.endToEnd = clTabbarOptionsExpanded.id
        layoutParams.topToBottom = previous.first.id
        clTabbarOptionsExpanded.addView(textView, layoutParams)

        return current
    }

    private fun setOptionListener(view: TextViewToogleWidget, function: () -> Unit) {
        view.onTextClickListener = { function.invoke() }
    }

    override fun getLayout(): Int = R.layout.layout_tabbar

    fun showTabbarShadow(show: Boolean = true) {
        if (show) {
            vTabbarHeaderShadow.visibility = View.INVISIBLE
            vTabbarIconShadow.visibility = View.INVISIBLE
            vTabbarHeaderShadowOverlap.visibility = View.VISIBLE
            vTabbarIconShadowOverlap.visibility = View.VISIBLE
        } else {
            vTabbarHeaderShadow.visibility = View.VISIBLE
            vTabbarIconShadow.visibility = View.VISIBLE
            vTabbarHeaderShadowOverlap.visibility = View.GONE
            vTabbarIconShadowOverlap.visibility = View.GONE
        }
    }
}
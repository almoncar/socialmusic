package es.babel.ema.presentation.ui.mainScreen


import es.babel.ema.presentation.base.BaseToolbarsViewModel
import es.babel.ema.presentation.ui.MainToolbarsViewModel

class MainScreenViewModel : BaseToolbarsViewModel<MainScreenState, MainScreenNavigator.Navigation>() {

    override fun onStartAnalytic() {
    }

    override fun createInitialViewState(): MainScreenState = MainScreenState()

    override fun onConfigureToolbars(mainToolbarsVm: MainToolbarsViewModel) {
        mainToolbarsVm.onActionShowToolbar(show = true, gone = true)
        mainToolbarsVm.onActionUpdateToolbar {
            it.copy(
                    backVisibility = true
            )
        }
        mainToolbarsVm.onActionShowTabbar(true)
    }

}
package es.babel.ema.presentation.base

import android.app.Activity
import android.content.Intent
import es.babel.ema.presentation.inject.injectionActivityModule
import es.babel.ema.presentation.model.ActivityResultHandlerModel
import com.carmabs.ema.R
import com.carmabs.ema.android.ui.EmaToolbarFragmentActivity
import org.kodein.di.Kodein

/**
 * Base activity for LDA application
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

abstract class BaseActivity : EmaToolbarFragmentActivity() {

    private val resultHandler: HashMap<Int, ActivityResultHandlerModel> = HashMap()

    override val overrideTheme: Boolean = true

    override fun injectActivityModule(kodein: Kodein.MainBuilder): Kodein.Module = injectionActivityModule(this)

    override fun getLayout(): Int = R.layout.activity_base

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val remove = resultHandler[requestCode]?.implementation?.invoke(requestCode, parseResultCode(resultCode), data)
                ?: false
        if (remove) removeActivityResultHandler(requestCode)
    }

    fun addActivityResultHandler(activityResultHandlerModel: ActivityResultHandlerModel) {
        resultHandler[activityResultHandlerModel.id] = activityResultHandlerModel
    }

    fun removeActivityResultHandler(id: Int) {
        resultHandler.remove(id)
    }

    private fun parseResultCode(code: Int): ActivityResultHandlerModel.Result {
        return when (code) {
            Activity.RESULT_OK -> ActivityResultHandlerModel.Result.Success
            Activity.RESULT_CANCELED -> ActivityResultHandlerModel.Result.Fail
            else -> ActivityResultHandlerModel.Result.Other(code)
        }
    }
}
package es.babel.ema.presentation.ui.mainScreen

import com.carmabs.ema.R
import com.carmabs.ema.core.state.EmaExtraData
import es.babel.ema.presentation.base.BaseToolbarsFragment
import es.babel.ema.presentation.ui.MainToolbarsViewModel
import org.kodein.di.generic.instance

class MainScreenViewFragment : BaseToolbarsFragment<MainScreenState, MainScreenViewModel, MainScreenNavigator.Navigation>() {

    override fun onLoading(data: EmaExtraData) {
    }

    override fun onNormal(data: MainScreenState) {
    }

    override fun onError(error: Throwable): Boolean {
        return false
    }

    override val inputStateKey: String? = null

    override fun getFragmentLayout(): Int = R.layout.fragment_main_screen

    override val viewModelSeed: MainScreenViewModel by instance()

    override val navigator: MainScreenNavigator by instance()

    override fun onSingleEvent(data: EmaExtraData) {
    }

    override fun onInitializedWithToolbarsManagement(viewModel: MainScreenViewModel, mainToolbarViewModel: MainToolbarsViewModel) {

    }


}
package es.babel.ema.presentation.navigation

import android.app.Activity
import androidx.navigation.NavController
import com.carmabs.ema.core.navigator.EmaBaseNavigator
import com.carmabs.ema.core.navigator.EmaNavigationState
import es.babel.ema.presentation.base.BaseNavigator

/**
 * Navigation on Home
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

class MainNavigator(override val navController: NavController) : BaseNavigator<MainNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState {
        object GoBack : MainNavigator.Navigation() {
            override fun navigateWith(navigator: EmaBaseNavigator<out EmaNavigationState>) {
                val nav = navigator as MainNavigator
                nav.toBack()
            }
        }
    }

    private fun toBack() {
        navigateBack()
    }

}
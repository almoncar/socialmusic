package es.babel.ema.presentation.ui.splash

import com.carmabs.ema.R
import com.carmabs.ema.core.state.EmaExtraData
import es.babel.ema.presentation.base.BaseFragment
import es.babel.ema.presentation.navigation.SplashNavigator
import org.kodein.di.generic.instance


class SplashViewFragment : BaseFragment<SplashState, SplashViewModel, SplashNavigator.Navigation>() {

    override val inputStateKey: String?
        get() = null

    override val viewModelSeed: SplashViewModel by instance()

    override val navigator: SplashNavigator by instance()

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_splash
    }

    override fun onInitialized(viewModel: SplashViewModel) {

    }

    override fun onNormal(data: SplashState) {
    }

    override fun onLoading(data: EmaExtraData) {
    }

    override fun onSingleEvent(data: EmaExtraData) {
    }

    override fun onError(error: Throwable): Boolean {
        return false
    }

}
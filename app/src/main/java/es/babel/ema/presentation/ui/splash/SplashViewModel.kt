package es.babel.ema.presentation.ui.splash

import com.babel.lineadirecta.SPLASH_DELAY
import es.babel.ema.presentation.base.BaseViewModel
import es.babel.ema.presentation.navigation.SplashNavigator
import kotlinx.coroutines.delay


class SplashViewModel : BaseViewModel<SplashState, SplashNavigator.Navigation>() {
    override fun onStartAnalytic() {
    }


    override fun createInitialViewState(): SplashState {
        return SplashState()
    }

    override fun onStartFirstTime(statePreloaded: Boolean) {
        super.onStartFirstTime(statePreloaded)
        executeUseCase {
            delay(SPLASH_DELAY)
            navigate(SplashNavigator.Navigation.LoginFromSplash)
        }
    }
}
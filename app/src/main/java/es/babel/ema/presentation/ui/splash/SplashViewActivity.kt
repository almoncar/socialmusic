package es.babel.ema.presentation.ui.splash

import android.os.Bundle
import com.carmabs.ema.R
import es.babel.domain.STRING_EMPTY
import es.babel.ema.presentation.base.BaseActivity


class SplashViewActivity : BaseActivity() {


    override fun getToolbarTitle(): String? {
        return STRING_EMPTY
    }

    override fun createActivity(savedInstanceState: Bundle?) {
        super.createActivity(savedInstanceState)
        hideToolbar()
    }

    override fun getNavGraph(): Int {
        return R.navigation.navigation_splash
    }
}
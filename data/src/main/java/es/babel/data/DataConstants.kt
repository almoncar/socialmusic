package es.babel.data

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2019-11-17
 */

const val HEADER_AUTHORIZATION = "Authorization"

const val TIMEOUT_RETROFIT = 90L
package es.babel.data.repository

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import es.babel.data.BuildConfig
import es.babel.data.HEADER_AUTHORIZATION
import es.babel.data.repository.net.MrAPI
import es.babel.data.repository.net.SecuredHttpClient
import es.babel.domain.repository.Repository
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2019-11-17
 */

class ApiLdaRepository(private val context: Context) : Repository {

    private lateinit var mrApi: MrAPI
    private var tokenLogin: String? = null
    private var retrofit: Retrofit = createRetrofit()
        set(value) {
            field = value
            mrApi = field.create(MrAPI::class.java)
        }

    init {
        retrofit = createRetrofit()
    }

    private fun createRetrofit(token: String? = null): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.URL_CIMA)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(createGson()))
                //something could be wrong around CallAdapter
                .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
                .client(createHttpClient(token))
                .build()
    }

    private fun createGson(): Gson {
        return GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .serializeNulls().create()
    }

    private fun createHttpClient(token: String? = null): OkHttpClient {

        val arrayInterceptors = mutableListOf<Interceptor>()

        val loggerInterceptor = HttpLoggingInterceptor()
        loggerInterceptor.level = BuildConfig.HTTP_LOG_LEVEL

        if (BuildConfig.DEBUG_HTTP_REQUEST) {
            arrayInterceptors.add(ChuckInterceptor(context))
        }

        token?.let {
            arrayInterceptors.add(createTokenHeaderInterceptor(it))
        }


        return SecuredHttpClient().getOkHttpClient(*arrayInterceptors.toTypedArray())
    }

    private fun createTokenHeaderInterceptor(token: String): Interceptor {
        return Interceptor {
            val newRequest = it.request()
                    .newBuilder()
                    .addHeader(HEADER_AUTHORIZATION, token)
                    .build()

            it.proceed(newRequest)
        }
    }
}
package es.babel.domain

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2019-11-19
 */

const val STRING_EMPTY = ""


const val INT_NEGATIVE = -1
const val FLOAT_ZERO = 0f
const val INT_ZERO = 0
package es.babel.domain.model

/**
 * Model to represent the compile environment
 *
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

enum class EnvironmentCompileType {
    MOCK,
    LOCAL
}
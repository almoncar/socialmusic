package es.babel.domain.model

/**
 * Model for app version
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

data class VersionModel(val code:Int, val name:String) {
}
package es.babel.domain.exceptions

/**
 * Class to handler error exception
 *
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

interface ErrorHandler {

    fun getException(code:Int):Throwable
    fun getVerificationLoginException(code: String): Throwable?
}
package es.babel.domain.exceptions

import es.babel.domain.INT_ZERO
import java.io.IOException

/**
 * Base Exception for LDA project
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

abstract class BaseException(val code: Int = INT_ZERO, val title: String, val description: String) : IOException(description)
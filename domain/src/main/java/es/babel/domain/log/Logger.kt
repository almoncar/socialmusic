package es.babel.domain.log

/**
 * Logger abstraction to log traces on app
 *
 * <p>
 * Copyright (c) 2018, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

interface Logger {

    fun d(message:String)
}
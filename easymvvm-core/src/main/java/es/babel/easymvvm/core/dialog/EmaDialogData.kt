package es.babel.easymvvm.core.dialog

/**
 * Interface to handle UI dialog data
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 */

interface EmaDialogData {
    val proportionWidth: Float?
    val proportionHeight: Float?
}
package es.babel.easymvvm.core.dialog

/**
 * Listener based on back OS button or confirm click
 *
 *
 * @author <a href="mailto:carlos.mateo@babel.es">Carlos Mateo Benito</a>
 */
interface EmaDialogListener {
    fun onBackPressed()
}